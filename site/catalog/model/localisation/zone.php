<?php
class ModelLocalisationZone extends Model {
	public function getZone($zone_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "zone WHERE zone_id = '" . (int)$zone_id . "' AND status = '1'");

		return $query->row;
	}

	public function getZonesByCountryId($country_id) {
		$zone_data = $this->cache->get('zone.' . (int)$country_id);

		if (!$zone_data) {
			$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "zone WHERE country_id = '" . (int)$country_id . "' AND status = '1' ORDER BY name");

			$zone_data = $query->rows;

			$this->cache->set('zone.' . (int)$country_id, $zone_data);
		}

		return $zone_data;
	}

    public function getZoneIdByCity($city) {
        $zone_data = $this->db->query("SELECT zone_id FROM " . DB_PREFIX . "city_to_zone WHERE city = '" . $this->db->escape($city) . "'")->row;
        return $zone_data ? $zone_data['zone_id'] : 0;
    }

    public function getZoneByCity($city) {
        $zone_data = $this->db->query("SELECT t1.city, t2.* FROM " . DB_PREFIX . "city_to_zone t1 LEFT JOIN " . DB_PREFIX . "zone t2 ON t1.zone_id = t2.zone_id where city='" . $this->db->escape($city)  . "' AND status = '1'")->row;
        return $zone_data;
    }
}