<?php
/**
 * @author Alex Tymchenko <adellantado@gmail.com>
 */

class Price {

    private $config;
    private $currency;

    public function __construct($registry) {
        $this->config = $registry->get('config');
        $this->currency = $registry->get('currency');
    }

    public function getFinalPrice($price, $currency_code) {

        if (isset($currency_code) && $currency_code != $this->config->get('config_currency')) {
            $price = $this->currency->convert((float)$price, $currency_code, $this->config->get('config_currency'));
        }

        $profit_rate = $this->config->get('config_profit_rate') ? (float)$this->config->get('config_profit_rate') : 1;

        return round($price * $profit_rate, 1, PHP_ROUND_HALF_UP);
    }

}