<?php
// HTTP
define('HTTP_SERVER', 'http://handmade.beedevs.com/');

// HTTPS
define('HTTPS_SERVER', 'http://handmade.beedevs.com/');

// DIR
define('DIR_APPLICATION', '/home/beedevs/beedevs.com/handmade/catalog/');
define('DIR_SYSTEM', '/home/beedevs/beedevs.com/handmade/system/');
define('DIR_LANGUAGE', '/home/beedevs/beedevs.com/handmade/catalog/language/');
define('DIR_TEMPLATE', '/home/beedevs/beedevs.com/handmade/catalog/view/theme/');
define('DIR_CONFIG', '/home/beedevs/beedevs.com/handmade/system/config/');
define('DIR_IMAGE', '/home/beedevs/beedevs.com/handmade/image/');
define('DIR_CACHE', '/home/beedevs/beedevs.com/handmade/system/cache/');
define('DIR_DOWNLOAD', '/home/beedevs/beedevs.com/handmade/system/download/');
define('DIR_UPLOAD', '/home/beedevs/beedevs.com/handmade/system/upload/');
define('DIR_MODIFICATION', '/home/beedevs/beedevs.com/handmade/system/modification/');
define('DIR_LOGS', '/home/beedevs/beedevs.com/handmade/system/logs/');

// DB
define('DB_DRIVER', 'mysqli');
define('DB_HOSTNAME', 'beedevs.mysql.ukraine.com.ua');
define('DB_USERNAME', 'beedevs_igor');
define('DB_PASSWORD', 'pass');
define('DB_DATABASE', 'beedevs_db');
define('DB_PORT', '3306');
define('DB_PREFIX', 'oc_');
